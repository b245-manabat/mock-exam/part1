function countLetter(letter, sentence) {
    let result = 0;
    let newLetter = letter.toLowerCase();
    let newSentence = sentence.toLowerCase();

    // Check first whether the letter is a single character.
    // If letter is a single character, count how many times a letter has occurred in a given sentence then return count.
    // If letter is invalid, return undefined.

    if(newLetter.length === 1){
        for(let i = 0; i < newSentence.length; i++) {

            if(newSentence[i] === newLetter) {
                result++;   
            }
        } return result;

    } else {
        return undefined;
    }
    
}

function isIsogram(text) {
    // An isogram is a word where there are no repeating letters.
    // The function should disregard text casing before doing anything else.
    // If the function finds a repeating letter, return false. Otherwise, return true.

    newText = text.toLowerCase();
    
    for (let i = 0;newText.length > i ; i++) {

        let repeater = 0

        for (let x = 0;newText.length > x ; x++) {
          
          if (newText[i] === newText[x]) {

                repeater++;
            
                if(repeater > 1){
                    return false
            }
          }
        }
      }
      return true

    
}

function purchase(age, price) {
    // Return undefined for people aged below 13.
    // Return the discounted price (rounded off) for students aged 13 to 21 and senior citizens. (20% discount)
    // Return the rounded off price for people aged 22 to 64.
    // The returned value should be a string.
    
    if (age >= 13){

        if(age <= 21) {

            let discounted = price * .8;
            let result = discounted.toFixed(2);

            return result;

        } else if (age < 65){

            return price.toFixed(2)
        }
        else {

            let discounted = price * .8;
            let result = discounted.toFixed(2);

            return result;

        }

    } else {
        return undefined
    }
    
    
}

function findHotCategories(items) {
    // Find categories that has no more stocks.
    // The hot categories must be unique; no repeating categories.

    // The passed items array from the test are the following:
//     
//     { id: 'tltry001', name: 'soap', stocks: 14, category: 'toiletries' },
//     { id: 'tltry002', name: 'shampoo', stocks: 8, category: 'toiletries' },
//     { id: 'tltry003', name: 'tissues', stocks: 0, category: 'toiletries' },
//     { id: 'gdgt001', name: 'phone', stocks: 0, category: 'gadgets' },
//     { id: 'gdgt002', name: 'monitor', stocks: 0, category: 'gadgets' }

    // The expected output after processing the items array is ['toiletries', 'gadgets'].
    // Only putting return ['toiletries', 'gadgets'] will not be counted as a passing test during manual checking of codes.
    let emptyArray = [];

    for (let i = 0;items.length > i; i++) {

        if (items[i].stocks === 0) {

            if (!emptyArray.includes(items[i].category)) {
                
                emptyArray.push(items[i].category);

            }
        }
    }
    return emptyArray;


}

function findFlyingVoters(candidateA, candidateB) {
    // Find voters who voted for both candidate A and candidate B.

    // The passed values from the test are the following:
    // candidateA: ['LIWf1l', 'V2hjZH', 'rDmZns', 'PvaRBI', 'i7Xw6C', 'NPhm2m']
    // candidateB: ['kcUtuu', 'LLeUTl', 'r04Zsl', '84EqYo', 'V2hjZH', 'LIWf1l']

    // The expected output after processing the candidates array is ['LIWf1l', 'V2hjZH'].
    // Only putting return ['LIWf1l', 'V2hjZH'] will not be counted as a passing test during manual checking of codes.

    let result = [];

    for(let i=0;candidateA.length > i;i++){
        for(let x=0;candidateB.length > x;x++){
            if(candidateA[i]===candidateB[x]){
                result.push(candidateA[i]);
            }
        }
    }

    return result;  
}

module.exports = {
    countLetter,
    isIsogram,
    purchase,
    findHotCategories,
    findFlyingVoters
};